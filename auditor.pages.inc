<?php

/**
 * @file
 * Page callbacks for auditor module.
 */

/**
 * Page callback for primary auditor page.
 */
function auditor_report_page() {
  $reports = auditor_get_reports();

  $results = array();
  foreach ($reports as $report) {
    $data = auditor_fetch_report_data($report);
    $results[$report['name']] = array('#pre_render' => array('auditor_build_report'),  '#data' => $data, '#report_plugin' => $report);
  }

  return $results;
}
