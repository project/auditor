<?php

// Plugin definition
$plugin = array(
  'title' => t('Variable count'),
  'description' => t('Provides a count of variables in the variables system.'),
  'instructions' => t('All variables are loaded in memory when Drupal bootstraps and unused variables for disabled or removed modules can consume extra memory.'),
  'data callback' => 'auditor_variable_count_report_data',
  'pre_render' => array('auditor_variable_count_report_build_output'),
  'columns' => array(
    'count' => t('Total variables'),
  ),
);
/**
 * Get a count of entities per entity type.
 */
function auditor_variable_count_report_data($report) {
  global $conf;

  $items = array(
    'variables' => array(
      'count' => count($conf),
    )
  );

  return array(
    'status' => 'info',
    'items' => $items,
  );
}

function auditor_variable_count_report_build_output($elements) {
  $report = $elements['#report_plugin'];
  $data = $elements['#data'];

  $elements['data'] = array(
    '#markup' => format_plural($data['items']['variables']['count'], '1 variable', '@count variables'),
    '#prefix' => '<h3>',
    '#suffix' => '</h3>',
  );

  return $elements;
}

