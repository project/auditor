<?php

// Plugin definition
$plugin = array(
  'title' => t('Disabled themes'),
  'description' => t('Provides a list of contributed themes that are disabled.'),
  'instructions' => t(''),
  'data callback' => 'auditor_disabled_theme_report_data',
  'pre_render' => array('auditor_build_table'),
  'columns' => array(
    'name' => t('Name'),
    'filename' => t('File name'),
    'version' => array('name' => t('Version')),
    'weight' => array('name' => t('Weight'), 'default' => FALSE),
  ),
);

/**
 * Get a count of entities per entity type.
 */
function auditor_disabled_theme_report_data($report) {
  $items = array();

  $themes = system_list('theme');
  foreach ($themes as $name => $theme_info) {
    if (preg_match('/^themes/', $theme_info->filename) == FALSE && $theme_info->status == FALSE) {
      $items[$name] = array(
        'name' => $theme_info->info['name'],
        'filename' => $theme_info->filename,
        'schema_version' => $theme_info->schema_version,
        'type' => $theme_info->type,
        'status' => $theme_info->status,
        'version' => $theme_info->info['version'],
        'weight' => $theme_info->weight,
      );
    }
  }

  return array(
    'status' => 'info',
    'items' => $items,
  );
}
