<?php

// Plugin definition
$plugin = array(
  'title' => t('System table'),
  'description' => t('Analyze data in the system table for missing modules and themes.'),
  'instructions' => t('The following items are present in the system table, but the corresponding files have been deleted. If the schema version if 0 or above, that module has been installed and may have data stored in the database.'),
  'data callback' => 'auditor_system_table_report_data',
  'pre_render' => array('auditor_build_table'),
  'columns' => array(
    'name' => t('Name'),
    'filename' => t('File name'),
    'schema_version' => t('Schema version'),
    'type' => t('Type'),
    'version' => array('name' => t('Version'), 'default' => FALSE),
    'weight' => array('name' => t('Weight'), 'default' => FALSE),
    'status' => array(
      'name' => t('Status'),
      'format' => 'boolean',
      'options' => array('false' => t('Disabled'), 'true' => t('Enabled')),
    ),
  ),
);

/**
 * Get a list of orphan items from the system table.
 */
function auditor_system_table_report_data($report) {
  $result = db_query("SELECT * from {system}");

  $items = array();
  foreach ($result as $record) {
    $record->info = unserialize($record->info);
    if (!file_exists($record->filename)) {
      $items[$record->name] = array(
        'name' => $record->info['name'],
        'filename' => $record->filename,
        'schema_version' => $record->schema_version,
        'type' => $record->type,
        'status' => $record->status,
        'version' => $record->info['version'],
        'weight' => $record->weight,
      );
    }
  }

  if (count($items) < 1) {
    $status = 'success';
  }
  else {
    $status = 'warning';
  }

  return array(
    'status' => $status,
    'items' => $items,
  );
}
