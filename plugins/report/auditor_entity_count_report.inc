<?php

// Plugin definition
$plugin = array(
  'title' => t('Entity count'),
  'description' => t('Provides a count of storied entities for each entity type.'),
  'instructions' => t(''),
  'data callback' => 'auditor_entity_count_report_data',
  'pre_render' => array('auditor_build_table'),
  'columns' => array(
    'name' => t('Entity type'),
    'module' => t('Module'),
    'count' => t('Total entities'),
  ),
);

/**
 * Get a count of entities per entity type.
 */
function auditor_entity_count_report_data($report) {
  $items = array();
  foreach (entity_get_info() as $type => $info) {
    $items[$type] = array(
      'count' => _auditor_entity_count_get_entity_count($type),
      'module' => isset($info['module']) ? $info['module'] : '-',
      'name' => $info['label'],
    );
  }
  return array(
    'status' => 'info',
    'items' => $items,
  );
}

/**
 * Return entity count for a given entity.
 *
 * @return integer A count of all entities of the given type.
 */
function _auditor_entity_count_get_entity_count($type) {
  $query = new EntityFieldQuery();
  return $query->entityCondition('entity_type', $type)->count()->execute();
}
